package com.gitlab.shogo.okazaki.MonkeyWithScala.ast

import com.gitlab.shogo.okazaki.MonkeyWithScala.token.Token

case class Identifier(token: Token, value: String) extends Expression {

  override def expressionNode(): Unit = {}

  /**
   * ノードが関連付けられているトークンのリテラル値を返す
   *
   * @return トークンのリテラル値
   */
  override def tokenLiteral(): String = {token.literal}

  override def String() = value
}
