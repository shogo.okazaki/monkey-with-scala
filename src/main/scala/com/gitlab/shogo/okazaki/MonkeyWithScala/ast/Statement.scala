package com.gitlab.shogo.okazaki.MonkeyWithScala.ast

/**
 * ノードのうち、Statement を表す
 */
trait Statement extends Node {
  def statementNode(): Unit
}
