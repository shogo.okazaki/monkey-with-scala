package com.gitlab.shogo.okazaki.MonkeyWithScala.ast

import com.gitlab.shogo.okazaki.MonkeyWithScala.token.Token

case class LetStatement(var token: Token, var name: Identifier, var value: Expression) extends Statement {

  override def statementNode(): Unit = {}

  override def String(): String = ???

  /**
   * ノードが関連付けられているトークンのリテラル値を返す
   *
   * @return トークンのリテラル値
   */
  override def tokenLiteral(): String = {
    token.literal
  }
}
