package com.gitlab.shogo.okazaki.MonkeyWithScala.ast

/**
 * ノードのうち、Expression を表す
 */
trait Expression extends Node {
  def expressionNode(): Unit
}
