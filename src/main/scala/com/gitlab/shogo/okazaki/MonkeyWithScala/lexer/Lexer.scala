package com.gitlab.shogo.okazaki.MonkeyWithScala.lexer

import com.gitlab.shogo.okazaki.MonkeyWithScala.token.{Token, TokenType}

/**
 * 字句解析器（レキサー）を定義するクラス
 *
 * @param input 入力された文字列
 */
case class Lexer(input: String) {

  /** 入力における現在の読み込み位置 */
  private var position: Int = 0

  /** 現在の次の読み込み位置 */
  private var readPosition: Int = 0

  /** 現在検査中の文字 */
  private var ch: Char = 0.toChar

  readChar()

  /**
   * 入力文字列を1文字読み込み、読み込み位置を変更する
   * @return 次の読み込み位置が文字列の長さを超えていた場合は、検査中の文字を 0 にしたレキサーを返却する。<br>
   *           それ以外の場合は、検査中文字を次の読み込み位置のものに指定する。<br>
   *             レキサーの現在の読み込み位置と次の読み込み位置は1つずつ進める。
   */
  def readChar(): Unit = {
    if (readPosition >= input.length) {
      ch = 0.toChar
    } else {
      ch = input(readPosition)
    }
    position = readPosition
    readPosition += 1
  }

  /**
   * 引数で指定したレキサーの次のトークンを返却する
   * @return 文字列の先頭に空白文字がある場合はを除く。<br>
   *           レキサーの現在の読み込み文字が所定のものならば、レキサーの読み込み位置を1文字進め、所定のオブジェクトを返却する。<br>
   *             読み込み文字が 0 の場合は、EOF を返却する。<br>
   *               読み込み文字が所定のものでない場合、英字か数字かを判定する。<br>
   *                 英字の場合、その文字がキーワードに指定されているかを判定した結果を返す。<br>
   *                   数字の場合は INT を返す。<br>
   *                     いずれでもない場合は、 ILLEGAL を返却する。
   */
  def nextToken(): Token = {

    skipWhitespace()

    val token: Token = ch match {
      case '=' =>
        if (peekChar() == '=') {
          val currentCh = this.ch
          readChar()
          val literal = currentCh.toChar.toString + this.ch.toChar.toString
          Token(TokenType.EQ, literal)
        } else {
          Token(TokenType.ASSIGN, ch)
        }
      case '+' => Token(TokenType.PLUS, this.ch)
      case '-' => Token(TokenType.MINUS, this.ch)
      case '!' =>
        if (peekChar() == '=') {
          val currentCh = ch
          readChar()
          val literal = currentCh.toString + this.ch.toString
          Token(TokenType.NOT_EQ, literal)
        } else {
          Token(TokenType.BANG, ch)
        }
      case '/' => Token(TokenType.SLASH, ch)
      case '*' => Token(TokenType.ASTERISK, ch)
      case '<' => Token(TokenType.LT, ch)
      case '>' => Token(TokenType.GT, ch)
      case ';' => Token(TokenType.SEMICOLON, ch)
      case '(' => Token(TokenType.LPAREN, ch)
      case ')' => Token(TokenType.RPAREN, ch)
      case ',' => Token(TokenType.COMMA, ch)
      case '{' => Token(TokenType.LBRACE, ch)
      case '}' => Token(TokenType.RBRACE, ch)
      case 0 => Token(TokenType.EOF,"")
      case _ =>
        if(isLetter(this.ch)) {
          val ident = readIdentifier()
          return Token(TokenType.lookupIdent(ident), ident)
        } else if(isDigit(this.ch)) {
          return Token(TokenType.INT, readNumber())
        } else {
          return Token(TokenType.ILLEGAL, this.ch)
        }
    }

    readChar()
    token
  }

  /**
   * レキサーに入力された文字列を識別子として読み込んだ結果を返却する
   * @return レキサー入力文字列を識別子として読み込んだ文字列
   */
  private def readIdentifier(): String = {
    val pos = position
    while(isLetter(ch)) {
      readChar()
    }
    input.substring(pos, position)
  }

  /**
   * レキサーに入力された文字列を数字の列として読み込んだ結果を返却する
   * @return レキサー入力文字列を数字列として読み込んだ文字列
   */
  private def readNumber(): String = {
    val pos = position
    while(isDigit(ch)) {
      readChar()
    }
    input.substring(pos, position)
  }

  /**
   * 指定されたバイト文字が英字（大文字・小文字）かアンダースコアのいずれかであるかを判定する
   * @param ch バイト文字
   * @return 英字かアンダースコアであった場合は True, そうでなければ False を返却する。
   */
  def isLetter(ch: Char): Boolean = {
    (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || ch == '_'
  }

  /**
   * 指定されたバイト文字が数字であるかを判定する
   * @param ch バイト文字
   * @return 数字の場合は True, そうでない場合は False を返却する。
   */
  def isDigit(ch: Char): Boolean = {
    ch >= '0' && ch <= '9'
  }

  /**
   * レキサーに入力された文字列の先頭の空白文字を除いたレキサーを返却する
   * @return 先頭の空白文字を除いたレキサー
   */
  def skipWhitespace(): Unit = {
    while(ch == ' '.toByte || ch == '\t'.toByte || ch == '\n'.toByte || ch == '\r'.toByte) {
      readChar()
    }
  }

  /**
   * レキサーに入力された文字列を先読みする
   * @return レキサーの次の読み込み位置が文字列の文字数を超えていた場合は 0 を返却する。<br>
   *           そうでない場合は、レキサーの次の読み込み位置の文字を先読みし、バイト文字に変換したものを返却する。<br>
   */
  def peekChar(): Char = {
    if (readPosition >= input.length) {
      0.toChar
    } else {
      input(readPosition)
    }
  }
}