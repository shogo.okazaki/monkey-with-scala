package com.gitlab.shogo.okazaki.MonkeyWithScala.parser

import com.gitlab.shogo.okazaki.MonkeyWithScala.ast.{Expression, Identifier, LetStatement, Program, ReturnStatement, Statement}
import com.gitlab.shogo.okazaki.MonkeyWithScala.lexer.Lexer
import com.gitlab.shogo.okazaki.MonkeyWithScala.token.{Token, TokenType}

/**
 * 構文解析器（パーサー）を表すクラス
 * @param lexer 字句解析器（レキサー）
 */
case class Parser(lexer: Lexer) {

  /** 現在のトークン */
  var curToken: Token = lexer.nextToken()

  /** 次のトークン */
  var peekToken: Token = lexer.nextToken()

  /** エラー内容 */
  var errors = List.empty[String]

  def peekError(tokenType: TokenType) = {
    val msg = s"expected next token to be $tokenType, got ${peekToken.`type`} instead"
    errors = errors :+ msg
  }

  /**
   * 繰り返しトークンを読み進めていく
   * @return EOF に達するまで入力のトークンを繰り返し読む。<br>
   *           構文解析するべきものが何も無くなったら、Program のルートノードを返却する。
   */
  def parseProgram(): Program = {
    var statements = List.empty[Statement]

    while(curToken.`type` != TokenType.EOF) {
      val statement = parseStatement()
      statement.foreach { stmt =>
        statements = statements :+ stmt
      }
      nextToken()
    }

    Program(statements)
  }

  /**
   * 構文解析を行う
   * @return
   */
  def parseStatement(): Option[Statement] = {
    curToken.`type` match {
      case TokenType.LET => parseLetStatement()
      case _ => None
    }
  }

  /**
   *
   * @return
   */
  def parseLetStatement(): Option[LetStatement] = {
    val stmtToken = curToken

    if (!expectPeek(TokenType.IDENT)) {
      return None
    }

    val name = Identifier(token = curToken, value = curToken.literal)

    if (!expectPeek(TokenType.ASSIGN)) {
      return None
    }

    // TODO: セミコロンに到達するまで読み飛ばしている
    while(curTokenIs(TokenType.SEMICOLON)) {
      nextToken()
    }

    val value: Expression = null

    Some(LetStatement(stmtToken, name, value))
  }

  /**
   * 与えられたパーサーに対して、トークンを1つ進めたパーサーを返却する
   * @return トークンを1つ進めたパーサー
   */
  def nextToken(): Unit = {
    curToken = peekToken
    peekToken = lexer.nextToken()
  }

  /**
   * 現在のトークンが引数で与えたトークンと同種かを判定する
   * @param tokenType トークンの種類
   * @return トークンの種類が一致する場合は True, そうでない場合は False を返却する
   */
  def curTokenIs(tokenType: TokenType): Boolean = {
    curToken.`type` == tokenType
  }

  /**
   * 次のトークンが引数で与えたトークンと同種かを判定する
   * @param tokenType トークンの種類
   * @return トークンの種類が一致する場合は True, そうでない場合は False を返却する
   */
  def peekTokenIs(tokenType: TokenType): Boolean = {
    peekToken.`type` == tokenType
  }

  /**
   *
   * @param tokenType
   * @return
   */
  def expectPeek(tokenType: TokenType): Boolean = {
    if (peekTokenIs(tokenType)) {
      nextToken()
      true
    } else {
      peekError(tokenType)
      false
    }
  }
}
