package com.gitlab.shogo.okazaki.MonkeyWithScala.ast

/**
 * 構文解析器のノードを表す
 */
trait Node {

  /**
   * ノードが関連付けられているトークンのリテラル値を返す
   * @return トークンのリテラル値
   */
  def tokenLiteral(): String
  def String(): String
}
