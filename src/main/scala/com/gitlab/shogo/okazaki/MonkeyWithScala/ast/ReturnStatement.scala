package com.gitlab.shogo.okazaki.MonkeyWithScala.ast

import com.gitlab.shogo.okazaki.MonkeyWithScala.token.Token

case class ReturnStatement(token: Token, value: Option[Expression]) extends Statement {
  override def statementNode(): Unit = {}

  override def tokenLiteral(): String = token.literal

  override def String(): String = ???
}
