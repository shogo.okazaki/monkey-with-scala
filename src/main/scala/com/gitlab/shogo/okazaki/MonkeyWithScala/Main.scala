package com.gitlab.shogo.okazaki.MonkeyWithScala

import com.gitlab.shogo.okazaki.MonkeyWithScala.repl.Repl

object Main extends App{
  val username = System.getProperty("user.name")

  println(
    s"""Hello ${username}! This is the Monkey proggraming language!
       |Feel free to type in commands
       |""".stripMargin
  )
  Repl.start()
}