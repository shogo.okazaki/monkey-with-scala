package com.gitlab.shogo.okazaki.MonkeyWithScala.token

/**
 * トークンの種類を表す列挙体
 */
sealed trait TokenType

object TokenType {
  object ILLEGAL extends TokenType
  object EOF extends TokenType

  // 識別子+リテラル
  object IDENT extends TokenType
  object INT extends TokenType

  // 演算子
  object ASSIGN extends TokenType
  object PLUS extends TokenType
  object MINUS extends TokenType
  object BANG extends TokenType
  object ASTERISK extends TokenType
  object SLASH extends TokenType
  object LT extends TokenType
  object GT extends TokenType
  object EQ extends TokenType
  object NOT_EQ extends TokenType

  // デリミタ
  object COMMA extends TokenType
  object SEMICOLON extends TokenType

  object LPAREN extends TokenType
  object RPAREN extends TokenType
  object LBRACE extends TokenType
  object RBRACE extends TokenType

  // キーワード
  object FUNCTION extends TokenType
  object LET extends TokenType
  object TRUE extends TokenType
  object FALSE extends TokenType
  object IF extends TokenType
  object ELSE extends TokenType
  object RETURN extends TokenType

  val keywords: Map[String, TokenType] = Map(
    "fn" -> FUNCTION,
    "let" -> LET,
    "true" -> TRUE,
    "false" -> FALSE,
    "if" -> IF,
    "else" -> ELSE,
    "return" -> RETURN,
  )

  /**
   * 引数で与えた識別子がキーワードの中にあるかを探す
   * @param ident 識別子
   * @return キーワード内にある場合はキーワードのオブジェクトを、ない場合は、IDENT を返却する
   */
  def lookupIdent(ident: String): TokenType = {
    keywords.getOrElse(ident, IDENT)
  }
}
