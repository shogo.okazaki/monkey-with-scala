package com.gitlab.shogo.okazaki.MonkeyWithScala.ast

/**
 * すべての AST のルートノード
 * @param statements ステートメントのリスト
 */
case class Program(statements: List[Statement]) extends Node{

  def tokenLiteral(): String = {
    if (statements.length > 0) {
      statements.head.tokenLiteral()
    } else {
      ""
    }
  }

  def String(): String = statements.foldLeft("")(_ + _.String())
}
