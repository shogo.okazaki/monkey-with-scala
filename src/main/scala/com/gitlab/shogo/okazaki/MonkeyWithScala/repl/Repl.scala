package com.gitlab.shogo.okazaki.MonkeyWithScala.repl

import com.gitlab.shogo.okazaki.MonkeyWithScala.lexer.Lexer
import com.gitlab.shogo.okazaki.MonkeyWithScala.token.TokenType

/**
 * REPL を起動するためのクラス
 */
object Repl {

  val PROMPT = ">> "

  def start() = {
    while(true) {
      val scanner = new java.util.Scanner(System.in)
      print(PROMPT)
      val lexer = Lexer(scanner.nextLine())
      var token = lexer.nextToken()

      while(token.`type` != TokenType.EOF) {
        token = lexer.nextToken()
        println(s"{Type: ${token.`type`} Literal:${token.literal}")
      }
    }
  }
}
