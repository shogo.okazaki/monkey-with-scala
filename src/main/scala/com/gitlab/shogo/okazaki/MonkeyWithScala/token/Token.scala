package com.gitlab.shogo.okazaki.MonkeyWithScala.token

/**
 * トークンを表すクラス
 * @param `type` トークンの種類
 * @param literal トーキンに入力する文字列
 */
case class Token(`type`: TokenType, literal: String)

object Token {
  def apply(`type`: TokenType, ch: Char): Token = Token(`type`, ch.toString)
}