package com.gitlab.shogo.okazaki.MonkeyWithScala.lexer

import com.gitlab.shogo.okazaki.MonkeyWithScala.token.{Token, TokenType}
import com.gitlab.shogo.okazaki.MonkeyWithScala.lexer.Lexer
import org.scalatest.freespec.AnyFreeSpec

class LexerSpec extends AnyFreeSpec {

  case class ExpectedValue(expectedType: TokenType, expectedLiteral: String)

  "Lexer クラス" - {

    "演算子、デリミタのインプットについて、トークンのタイプと文字列が正しく対応していること" in {

      val input = "=+(){},;!-/*<>"
      val expectedValues: Array[ExpectedValue] = Array(
        ExpectedValue(TokenType.ASSIGN, "="),
        ExpectedValue(TokenType.PLUS, "+"),
        ExpectedValue(TokenType.LPAREN, "("),
        ExpectedValue(TokenType.RPAREN, ")"),
        ExpectedValue(TokenType.LBRACE, "{"),
        ExpectedValue(TokenType.RBRACE, "}"),
        ExpectedValue(TokenType.COMMA, ","),
        ExpectedValue(TokenType.SEMICOLON, ";"),
        ExpectedValue(TokenType.BANG, "!"),
        ExpectedValue(TokenType.MINUS, "-"),
        ExpectedValue(TokenType.SLASH, "/"),
        ExpectedValue(TokenType.ASTERISK, "*"),
        ExpectedValue(TokenType.LT, "<"),
        ExpectedValue(TokenType.GT, ">"),
        ExpectedValue(TokenType.EOF, ""),
      )
      var lexer = Lexer(input)

      expectedValues.foreach( expect => {
        val token = lexer.nextToken()
        assert(token.`type` == expect.expectedType)
        assert(token.literal == expect.expectedLiteral)
      })
    }

    "Monkey のソースコードに対して、正しく字句解析ができていること" in {

      val input =
        """let five = 5;
          |let ten = 10;
          |
          |let add = fn(x, y) {
          | x + y;
          |};
          |
          |let result = add(five, ten);
          |
          |if (5 < 10) {
          | return true;
          |} else {
          | return false;
          |}
          |""".stripMargin

      val expectedValues: Array[ExpectedValue] = Array(
        ExpectedValue(TokenType.LET, "let"),
        ExpectedValue(TokenType.IDENT, "five"),
        ExpectedValue(TokenType.ASSIGN, "="),
        ExpectedValue(TokenType.INT, "5"),
        ExpectedValue(TokenType.SEMICOLON, ";"),
        ExpectedValue(TokenType.LET, "let"),
        ExpectedValue(TokenType.IDENT, "ten"),
        ExpectedValue(TokenType.ASSIGN, "="),
        ExpectedValue(TokenType.INT, "10"),
        ExpectedValue(TokenType.SEMICOLON, ";"),
        ExpectedValue(TokenType.LET, "let"),
        ExpectedValue(TokenType.IDENT, "add"),
        ExpectedValue(TokenType.ASSIGN, "="),
        ExpectedValue(TokenType.FUNCTION, "fn"),
        ExpectedValue(TokenType.LPAREN, "("),
        ExpectedValue(TokenType.IDENT, "x"),
        ExpectedValue(TokenType.COMMA, ","),
        ExpectedValue(TokenType.IDENT, "y"),
        ExpectedValue(TokenType.RPAREN, ")"),
        ExpectedValue(TokenType.LBRACE, "{"),
        ExpectedValue(TokenType.IDENT, "x"),
        ExpectedValue(TokenType.PLUS, "+"),
        ExpectedValue(TokenType.IDENT, "y"),
        ExpectedValue(TokenType.SEMICOLON, ";"),
        ExpectedValue(TokenType.RBRACE, "}"),
        ExpectedValue(TokenType.SEMICOLON, ";"),
        ExpectedValue(TokenType.LET, "let"),
        ExpectedValue(TokenType.IDENT, "result"),
        ExpectedValue(TokenType.ASSIGN, "="),
        ExpectedValue(TokenType.IDENT, "add"),
        ExpectedValue(TokenType.LPAREN, "("),
        ExpectedValue(TokenType.IDENT, "five"),
        ExpectedValue(TokenType.COMMA, ","),
        ExpectedValue(TokenType.IDENT, "ten"),
        ExpectedValue(TokenType.RPAREN, ")"),
        ExpectedValue(TokenType.SEMICOLON, ";"),
        ExpectedValue(TokenType.IF, "if"),
        ExpectedValue(TokenType.LPAREN, "("),
        ExpectedValue(TokenType.INT, "5"),
        ExpectedValue(TokenType.LT, "<"),
        ExpectedValue(TokenType.INT, "10"),
        ExpectedValue(TokenType.RPAREN, ")"),
        ExpectedValue(TokenType.LBRACE, "{"),
        ExpectedValue(TokenType.RETURN, "return"),
        ExpectedValue(TokenType.TRUE, "true"),
        ExpectedValue(TokenType.SEMICOLON, ";"),
        ExpectedValue(TokenType.RBRACE, "}"),
        ExpectedValue(TokenType.ELSE, "else"),
        ExpectedValue(TokenType.LBRACE, "{"),
        ExpectedValue(TokenType.RETURN, "return"),
        ExpectedValue(TokenType.FALSE, "false"),
        ExpectedValue(TokenType.SEMICOLON, ";"),
        ExpectedValue(TokenType.RBRACE, "}"),
        ExpectedValue(TokenType.EOF, ""),
      )

      var lexer = Lexer(input)
      expectedValues.foreach( expect => {
        val token = lexer.nextToken()
        assert(token.`type` == expect.expectedType)
        assert(token.literal == expect.expectedLiteral)
      })
    }

    "2文字からなるトークンを正しく字句解析ができていること" in {
      val input =
        """10 == 10;
          |10 != 9;
          |""".stripMargin

      val expectedValues: Array[ExpectedValue] = Array(
        ExpectedValue(TokenType.INT, "10"),
        ExpectedValue(TokenType.EQ, "=="),
        ExpectedValue(TokenType.INT, "10"),
        ExpectedValue(TokenType.SEMICOLON, ";"),
        ExpectedValue(TokenType.INT, "10"),
        ExpectedValue(TokenType.NOT_EQ, "!="),
        ExpectedValue(TokenType.INT, "9"),
        ExpectedValue(TokenType.SEMICOLON, ";"),
      )

      var lexer = Lexer(input)
      expectedValues.foreach( expect => {
        val token = lexer.nextToken()
        assert(token.`type` == expect.expectedType)
        assert(token.literal == expect.expectedLiteral)
      })
    }
  }
}
