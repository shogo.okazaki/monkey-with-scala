package com.gitlab.shogo.okazaki.MonkeyWithScala.token

import org.scalatest.freespec.AnyFreeSpec

class TokenTypeSpec extends AnyFreeSpec{
  "TokenType クラス" - {
    "lookupIdent メソッド" - {
      "文字列「let」が与えられたときに、LET を返却すること" in {
        val result = TokenType.lookupIdent("let")
        val expect = TokenType.LET
        assert(result == expect)
      }

      "文字列「fn」が与えられたときに、FUNCTION を返却すること" in {
        val result = TokenType.lookupIdent("fn")
        val expect = TokenType.FUNCTION
        assert(result == expect)
      }

      "キーワードにない文字列が与えられたときに、IDENT を返却すること" in {
        val result = TokenType.lookupIdent("hoge")
        val expect = TokenType.IDENT
        assert(result == expect)
      }
    }
  }
}
