package com.gitlab.shogo.okazaki.MonkeyWithScala.parser

import com.gitlab.shogo.okazaki.MonkeyWithScala.ast.{Expression, LetStatement, Statement}
import com.gitlab.shogo.okazaki.MonkeyWithScala.lexer.Lexer
import com.gitlab.shogo.okazaki.MonkeyWithScala.token.TokenType
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

class ParserSpec extends AnyFreeSpec with Matchers {

  "Parser クラス" - {
    "Let statement をパースできること" in {

      val tests = List(
        ("let x = 5;", "x", 5),
        ("let y = true;", "y", true),
        ("let foobar = y;", "foobar", "y"),
      )

      tests.foreach {
        case (input, expectedIdentifier, expectedValue) => {
          val l = Lexer(input)
          val p = Parser(l)
          val program = p.parseProgram()
          println(s"parser: $p, curToken: ${p.curToken}, program: $program")
          checkParserErrors(p)

          assert(program.statements.length == 1)

          val stmt = program.statements.head
          testLetStatement(stmt, expectedIdentifier)

          //          val letStmt = stmt.asInstanceOf[LetStatement]
          //          val value   = letStmt.value
          //          testLiteralExpression(value, expectedValue)
        }
      }
    }
  }

  def checkParserErrors(p: Parser): Unit = {
    val errors = p.errors

    if (errors.isEmpty) {
      return
    }

    info(s"parser has ${errors.length} errors")
    errors.foreach(err => info(s"parser error: $err"))
    assert(errors.length == 0)
  }

  def testLetStatement(s: Statement, name: String): Unit = {
    assert(s.tokenLiteral() == "let")
    s shouldBe a[LetStatement]

    val letStmt = s.asInstanceOf[LetStatement]
    assert(letStmt.name.value == name)
    assert(letStmt.name.tokenLiteral() == name)
  }

//  def testLiteralExpression(maybeExp: Option[Expression], expected: Any): Unit = maybeExp match {
//    case Some(exp) =>
//      expected match {
//        case _: Int     => testIntegerLiteral(exp, expected.asInstanceOf[Int])
//        case _: Long    => testIntegerLiteral(exp, expected.asInstanceOf[Long])
//        case _: String  => testIdentifier(exp, expected.asInstanceOf[String])
//        case _: Boolean => testBooleanLiteral(exp, expected.asInstanceOf[Boolean])
//      }
//    case None => fail("Expression is empty")
//  }
}
